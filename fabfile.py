# -*- coding: utf-8 -*-

from fabric.api import *
from fabric.colors import *
from fabric.contrib.console import confirm

import os
import sys

from egorch.settings import DATABASES

'''
перед использованием запустить виртуальную машину
сборка файлов и автоматический деплой
fab -H localhost dep
fab -H localhost deploy
fab -H localhost test1
fab test1
'''


# Списком можно перечислить несколько серверов, которые у вас считаются "продакшеном"
env.roledefs['production'] = ['egor@185.236.78.105']
local_app = {}
local_app['project_root'] = '/home/egor/pythonwork/django_app/egorch'
local_app['backup_root'] = '/home/egor/pythonwork/backup/egorch/'


def production_env():
    """Окружение для продакшена"""
    #env.key_filename = [os.path.join(os.environ['HOME'], '.ssh', 'git_rsa')]  # Локальный путь до файла с ключами  
    env.user = 'egor'  # На сервере будем работать из под пользователя "git"
    env.project_root = '/var/www/egor/data/django/egorch'  # Путь до каталога проекта (на сервере)
    env.shell = '/bin/bash -c'  # Используем шелл отличный от умолчательного (на сервере)
    env.python = '/var/www/egor/data/virtualenv/env3.5/bin/python'  # Путь до python (на сервере)
    env.pip = '/var/www/egor/data/virtualenv/env3.5/bin/pip3'

def test1():
    '''тестовая задача'''
    print('тестовая задача')
    print(blue('blue'))
    print(cyan('cyan'))
    print(green('green'))
    print(magenta('magenta'))
    print(red('red'))
    print(white('white'))
    print(yellow('yellow'))
    
    with lcd(local_app['project_root']):
        print('расположение питона', sys.executable)
        local('python -V') 



def test():
    print(cyan('тестирование'))
    with lcd(local_app['project_root']):
        with settings(warn_only=True):
            result = local('python3 manage.py test egorch', capture=True)
        if result.failed and not confirm("Тесты содержат ошибку. Продолжить работу?"):
            abort("Прерывание по запросу пользователя")

def pip_install():
    print(cyan('pip: собираю все пакеты'))
    with lcd(local_app['project_root']):
        local("pip3 freeze > requirements.txt")

'''
def commit(arg1='сборка пакетов + коммит'):
    #локальная сборка пакетов
    with lcd(local_app['project_root']):
        local("git add -A")
        local("git commit -m '{commit}'".format(commit=arg1))
'''

def commit():
    print(cyan('коммит'))
    with lcd(local_app['project_root']):
        local("git add -A")
        local("git commit")
        
def push():
    print(cyan('push'))
    with lcd(local_app['project_root']):        
        local("git push origin master")
    

def dep():
    '''print(blue('локальная сборка пакетов'))'''
    #test()
    pip_install
    commit()
    push()
    
@roles('production')
def deploy():
    print(blue('удаленная установка пакетов + разворачивание приложения'))
    production_env()
    print("Выполняется на %s as %s" % (env.host, env.user))
    print('pip установка пакетов')
    with cd(env.project_root):
        #run('{python} -V'.format(python=env.python))       
        run('git pull origin master')
        #run('find . -name "*.mo" -print -delete')  # Чистим старые скомпиленные файлы gettext'а
        #run('{} manage.py compilemessages'.format(env.python))  # Собираем новые файлы gettext'а
        run('{} manage.py collectstatic --noinput'.format(env.python))  # Собираем статику
        run('{pip} install -r requirements.txt'.format(pip=env.pip))
        run('{python} manage.py migrate'.format(python=env.python))
        print('конец )')
        
def dump_developer():
    '''
    дамп локальной базы данных
    '''
    db = DATABASES['default']

    with lcd(local_app['backup_root']):
        #count = local('$(($(ls |wc -l) + 1));')
        arr = os.listdir(local_app['backup_root'])
        count = len(arr) + 1
        
        print('PGPASSWORD="{password}" pg_dump -h localhost -p 5432 -U {user} -C -F p -b -v -f egorch_django{count}.backup {name}'.format(
           user = db['USER'],
           password = db['PASSWORD'],
           name = db['NAME'],
           count=count,
        ))
        
    
          