$(document).ready(function(){
        
    $.fn.serializeObject = function()
    {
       var o = {};
       var a = this.serializeArray();
       $.each(a, function() {
           if (o[this.name] !== undefined) {
               if (!o[this.name].push) {
                   o[this.name] = [o[this.name]];
               }
               o[this.name].push(this.value || '');
           } else {
               o[this.name] = this.value || '';
           }
       });
       return o;
    };  
  
    $(".vote #button-vote").click(function(){
    	var data = {};
        data.category = $(this).attr("data-value");
        console.log(data.category);
        //data.csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
        //$("table.vote").children().remove();
		$.ajax({
			datatype: 'json',
			type: 'GET',
			url: '/vote-category-ajax',
			data: data,			
			success: function (result, status, xhr) {        
				result = $.parseJSON(result);
				console.log(result);
				//очистка блока с голосованием
				
				$.each(result, function(i){					
					$("table.vote tr:eq(" + i + ") td:eq(0)").html(result[i].title + ': ' + result[i].vote + '%');					
					$("table.vote tr:eq(" + i + ") #button-vote").attr("data-url", result[i].url);
					//$("table.vote tr:eq(" + i + ") button").attr("data-url", result[i].url).html(result[i].vote + '%');
				});
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("\n\t"  + textStatus + "\n\t" + errorThrown );
			},
		});
        return false;
    });
    
    // при наведении на звёздочку
    $("#rating-page img").hover(function(){   
        var starActive = $("#rating-page").attr('data-url') + 'star_rating_active2.png';
        var star = $("#rating-page").attr('data-url') + 'star_rating2.png';
        var score = $(this).attr('data-rating');
        $("#rating-page img:lt("+ score +")").attr('src', starActive).css("cursor", "pointer");
        $("#rating-page img:gt("+ (score - 1) +")").attr('src', star);
        return false;
    });
    
    // при отведении от звёздочки
    $("#rating-page img").mouseout(function(){
        var star = $("#rating-page").attr('data-url') + 'star_rating2.png';
        $("#rating-page img").attr('src', star);
        return false;
    });
    
    // клик по звёздочке
    $("#rating-page img").click(function(){   
        data = {};
        data.score = $(this).attr('data-rating');
        data.id = $("#rating-page").attr('data-id-page');                
        console.log(data);             
        
        $.ajax({
              datatype: 'json',
              type: 'GET',
              url: '/rating-post-ajax',
              data: data,
              success: function (result, status, xhr) {        
                  result = $.parseJSON(result);
                  if(result.error){ 
                      error_comment(result.error);
                  }else{
                      console.log(result); 
                      $(".rating-alert").show().fadeOut(2000);
                      $("#rating-page").fadeOut(2000);
                      $("#rating-num-info").text(result.rating_result);                                               
                  }  
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
                  console.log("\n\t"  + textStatus + "\n\t" + errorThrown );
              },
          });
        
        return false;
    });
	
	/* подсчёт кол-ва онлайн пользователей */
    setInterval(function(){        
        $.ajax({
          datatype: 'json',
          type: 'GET',
          url: '/online-user-ajax',          
          success: function (result, status, xhr) {        
              result = $.parseJSON(result);
              if(result.error){ 
                  error_comment(result.error);
              }else{
                  //console.log(result); 
                  $("#online-user-num").text(result.online_user);                                                 
              }  
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
              console.log("\n\t"  + textStatus + "\n\t" + errorThrown );
          },
       });
                    
    }, 10000);
	
	/* капча */
	$('.js-captcha-refresh').click(function(){
		$.ajax({
          datatype: 'json',
          type: 'GET',
          url: '/replace-captcha',          
          success: function (result, status, xhr) {        
              result = $.parseJSON(result);
              //console.log(result.new_cptch_image); 
              $("img.captcha").attr('src', result.new_cptch_image);           
              $("#id_captcha_0").val(result.new_cptch_key);                                                 
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
              console.log("\n\t"  + textStatus + "\n\t" + errorThrown );
          },
	    });
			
	    return false;
	});	
		
		
});