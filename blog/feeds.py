from django.contrib.syndication.views import Feed
from . models import Post
from django.utils.feedgenerator import Atom1Feed

class RssSiteNewsFeed(Feed):
    title = "канал новостей egorch.ru"
    link = "/sitenews/"
    description = "новые статьи сайта egorch.ru"

    def items(self):
        return Post.objects.order_by('-create_date')[:100]

class AtomSiteNewsFeed(RssSiteNewsFeed):
    feed_type = Atom1Feed
    subtitle = RssSiteNewsFeed.description