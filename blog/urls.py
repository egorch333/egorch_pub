from django.urls import path, re_path
from django.contrib.sitemaps.views import sitemap
from . sitemaps import BlogSitemap
from . import views
from . feeds import RssSiteNewsFeed, AtomSiteNewsFeed
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

sitemaps = {
    'post': BlogSitemap,
    }


app_name = 'blog'
urlpatterns = [
    path('',                     views.IndexView.as_view(), name='index'),
    path('page/<slug:url>/',     views.PageView.as_view(), name='page'),
    path('category/<slug:url>/', views.CategoryView.as_view(), name='category'),
    path('post/<slug:url>/',     views.PostView.as_view(), name='post'),
    path('add-comment/', views.AddCommentPostView.as_view(), name='add_comment'),
    path('sitemap/', views.SitemapView.as_view(), name='sitemap'),
    path('search/', views.SearchView.as_view(), name='search'),
    path('contact/', views.ContactView.as_view(), name='contact'),    
    path('replace-captcha/', views.ReplaceCaptchaAjaxView.as_view(), name='replace_captcha'),  
] 

#redirect
urlpatterns += [
    path('view_post.php', views.RedirectPhpPostView.as_view(), name='view_post'),    
    path('view_cat.php', views.RedirectPhpCategoryView.as_view(), name='view_category'),  
    path('redirect/<slug:title>/', views.RedirectView.as_view(), name='redirect'),    
    re_path('django.wsgi', views.RedirectWsgiView.as_view(), name='redirect_wsgi'),        
]

#ajax
urlpatterns += [
    path('rating-post-ajax/', views.RatingPostView.as_view(), name='rating_post_ajax'),
    path('vote-category-ajax/', views.VoteCategoryAjaxView.as_view(), name='vote_category_ajax'),
    path('online-user-ajax/', views.OnlineUserView.as_view(), name='online_user_ajax'),
]

#xml страницы
urlpatterns += [
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    path('sitenews/rss/', RssSiteNewsFeed()),
    path('sitenews/atom/', AtomSiteNewsFeed()),
]


if settings.DEBUG is True:   #if DEBUG is True it will be served automatically
    #отображение статических картинок + файлы медиа
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += staticfiles_urlpatterns()

