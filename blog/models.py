import datetime
from django.db import models
from django.utils import timezone
from pytils import translit
from django.utils.html import format_html

class Category(models.Model):
    url = models.CharField(max_length=200, unique=True, help_text='символы от 1 до ~ и текст a-z0-9')
    title = models.CharField(max_length=200, verbose_name='название')
    h1_title = models.CharField(max_length=200, blank=True, verbose_name='h1')
    keywords = models.CharField(max_length=200, verbose_name='ключевые слова')
    description = models.TextField(verbose_name='описание')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения')
    vote = models.IntegerField(default=0, help_text='%', verbose_name='голосование')
    
    #get_latest_by = "change_date" - сортировка для последнего значения
    #verbose_name = 'категория'
    #verbose_name_plural = 'категории'
    
    #для админки и shell
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        db_table = 'category' #название таблицы 
        ordering = ['title'] # сортировка по умолчанию для всех страниц view
    '''
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'
    '''
    
class Page(models.Model):
    url = models.SlugField(max_length = 200,  unique=True, help_text='символы от 1 до ~ и текст a-z0-9')
    title = models.CharField(max_length=200, blank=True, verbose_name='название')
    h1_title = models.CharField(max_length=200, blank=True, verbose_name='h1')
    keywords = models.CharField(max_length=200, blank=True, verbose_name='ключевые слова')
    text = models.TextField(blank=True, verbose_name='текст')
    description = models.TextField(blank=True, verbose_name='описание')
    view_page = models.IntegerField(default=0, verbose_name='кол-во просмотров')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения') 
    
    POSITION_CHOICES = (
        (u'0', 0),
        (u'1', 1),
        (u'2', 2),
        (u'3', 3),
        (u'4', 4),
        (u'5', 5),
        (u'6', 6),
        (u'7', 7),
        (u'8', 8),
        (u'9', 9),
    )
    position = models.CharField(max_length=1, choices=POSITION_CHOICES, default=0, verbose_name='позиция')
    
    #для админки и shell
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'Страница'
        verbose_name_plural = 'Страницы'
        db_table = 'page' #название таблицы 
        ordering = ['title'] # сортировка по умолчанию для всех страниц view


def upload_path_handler(instance, filename):
    '''
    создает папку согласно url, для post
    применяется транслитерация
    '''
    #картинка
    filename = filename.replace('_', '-')
    arr = filename.split('.')
    extention = arr.pop()
    image_name = ''.join(arr).replace('.', '')
    image_name = translit.slugify(image_name).replace('-', '_')
    filename = image_name + '.' + extention    
    
    #путь
    url = instance.url.replace('-', '_')
    return "post/{url}/{file}".format(url=url, file=filename)

class Post(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=1, verbose_name='категория')
    url = models.SlugField(max_length = 200, unique=True, help_text='символы от 1 до ~ и текст a-z0-9')
    title = models.CharField(max_length=200, blank=True, verbose_name='название')
    h1_title = models.CharField(max_length=200, blank=True, verbose_name='h1')
    keywords = models.CharField(max_length=250, blank=True, verbose_name='ключевые слова')
    #mini_icon = models.CharField(max_length=200, blank=True verbose_name='Ссылка картинки')
    mini_icon = models.ImageField(upload_to=upload_path_handler, default='post/icon.svg', verbose_name='минииконка')
    text = models.TextField(blank=True, verbose_name='текст')
    description = models.CharField(max_length=500, blank=True, verbose_name='описание')
    description_full = models.TextField(blank=True, verbose_name='описание:категория')
    visible = models.BooleanField(default=True, verbose_name='видимость')
    view_page = models.IntegerField(default=0, verbose_name='кол-во просмотров')
    author = models.CharField(max_length=100, default='Егор Астапов', verbose_name='автор')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения')
    #рейтинг
    rating = models.IntegerField(default=0)
    q_vote = models.IntegerField(default=0)
    rating_result = models.IntegerField(default=0, help_text='рейтинг 0-5', verbose_name='рейтинг')
    
    #для админки и shell
    def __str__(self):
        return self.title
    
    #для карты сайта xml
    def get_absolute_url(self):
        return "/post/%s/" % self.url
    
 
    def image_img(self):
        if self.mini_icon:
            # без format_html работать не будет
            return format_html("<a href='{0}' target='_blank'><img src='{0}' width='30'/></a>".format(self.mini_icon.url))
        else:
            return u"(Нет изображения)"
    image_img.short_description = 'минииконка'
    image_img.allow_tags = True
    
    
    def count_comment(self):
        #кол-во комментариев
        if self.id:
            return CommentPost.objects.filter(post_id=self.id).count()
        else:
            return 0
    count_comment.short_description = 'кол-во комментариев'
    count_comment.allow_tags = True
    #count_comment.admin_order_field = 'count_comment' - создать триггер


    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'
        db_table = 'post' #название таблицы 
        ordering = ['title'] # сортировка по умолчанию для всех страниц view
        
class SitesFriends(models.Model):
    url = models.CharField(max_length = 200, help_text='символы от 1 до ~ и текст a-z0-9')
    title = models.CharField(max_length=200, blank=True, verbose_name='название')
    text = models.CharField(max_length=200, blank=True, verbose_name='текст')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения')
    visible = models.BooleanField(default=True, verbose_name='видимость')
    
    #для админки и shell
    def __str__(self):
        return self.text
    
    class Meta:
        verbose_name = 'Сайты друзей'
        verbose_name_plural = 'Сайты друзей'
        db_table = 'sites_friends' #название таблицы 
        ordering = ['text'] # сортировка по умолчанию для всех страниц view        
        
class CommentPost(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, default=1, verbose_name='статья')
    name_user = models.CharField(max_length=200, blank=True, verbose_name='имя пользователя')
    text = models.TextField(blank=True, verbose_name='текст')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    #если нужно редактировать
    #create_date.editable=True
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения')
    
    #для админки и shell
    def __str__(self):
        return self.text
    
    class Meta:
        verbose_name = 'Комментарий к статье'
        verbose_name_plural = 'Комментарии к статье'
        db_table = 'comment_post' #название таблицы 
        ordering = ['-create_date'] # сортировка по умолчанию для всех страниц view 
        
class OnlineUser(models.Model):
    ip = models.CharField(max_length=50, blank=True)
    create_date = models.DateTimeField(auto_now_add=True)
    #create_date.editable=True
    
    class Meta:        
        db_table = 'online_user'
        
class Redirect(models.Model):
    title = models.CharField(max_length=200, blank=True, help_text="символы en 0-9", verbose_name='название')    
    url = models.URLField(max_length = 200, help_text='текст en + 0-9')
    description = models.CharField(max_length=200, blank=True, help_text="краткое описание 200 символов", verbose_name='описание')
    count_redirect = models.IntegerField(default=0, verbose_name='кол-во переходов')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения')
    
    #для админки и shell
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = 'Редирект'
        verbose_name_plural = 'Редирект'
        db_table = 'redirect' #название таблицы 
        ordering = ['title'] # сортировка по умолчанию для всех страниц view 
        
        
class StopWord(models.Model):
    text = models.TextField(blank=True, verbose_name='текст')    
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения')
    
    class Meta:
        verbose_name = 'Стоп слова'
        verbose_name_plural = 'Стоп слова'
        db_table = 'stop_word' #название таблицы        
        
           