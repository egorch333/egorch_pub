# Generated by Django 2.0 on 2018-02-10 00:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20180209_2026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commentpost',
            name='create_date',
            field=models.DateTimeField(),
        ),
    ]
