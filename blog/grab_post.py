# -*- coding: utf-8

import MySQLdb
import re
import os
from .models import *

import pytz
from datetime import datetime



#подключение
'''
python manage.py shell
import blog.grab_post

Скрипт для копирования статей post с удалённого сервера
exit()
'''

#подключён файл
print('подключён файл: ' + __file__)


# подключаемся к базе данных (не забываем указать кодировку, а то в базу запишутся иероглифы)
db = MySQLdb.connect(host="localhost", user="root", passwd="123456", db="egorch", charset='utf8')
# формируем курсор, с помощью которого можно исполнять SQL-запросы
cursor = db.cursor()
#print("Версия MySQL :" + str(data))
# проверим таблицы
cursor.execute("SELECT * FROM data order by id")


def parse_url(text, url):
    '''
    меняем урл в тексте
    '''
    text = text.replace('view_post.php?id=', 'post/')
    text = text.replace('http://egorch.ru/files/', 'files/')
    path = '/media/post/' + str(url) + '/' 
    result = re.sub(r'files/\w+/\w+/(\w+/)?(\w+/)?(\w+/)?', path, text)
    result = result.replace('//media', '/media')
    return result

def replace_category(id):
    '''
    меняем id
    '''
    if id == 14:
       id = 13 
    return id

for row in cursor.fetchall():
    page = list(row)
    
    if page[0] != 1:
        q = Post()
        q.url = str(page[0])    
        q.category_id = replace_category(page[1])
        q.description = page[2]
        q.keywords =  page[3][0:250]
        q.description_full =  page[4]
        q.text = parse_url(page[5], q.url)
        q.view_page =  page[6]
        q.author =  page[7]
        q.create_date = datetime.strptime(str(page[8]), '%Y-%m-%d') 
        print(q.create_date)
        #q.create_date = page[8] #доделать 
        q.mini_icon =  parse_url(page[9], q.url)
        q.title =  page[10]
        q.h1_title =  page[10] 
        q.rating =  page[12]
        q.q_vote =  page[13]
        rating_result = q.rating / q.q_vote
        q.rating_result =  int(rating_result)
        
        q.save()
    
db.close()