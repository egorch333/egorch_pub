# -*- coding: utf-8

import MySQLdb
import re
import os
from .models import *

import pytz
from datetime import datetime



#подключение
'''
python manage.py shell
import blog.grab_comment

Скрипт для копирования статей post с удалённого сервера
exit()
'''

#подключён файл
print('подключён файл: ' + __file__)


# подключаемся к базе данных (не забываем указать кодировку, а то в базу запишутся иероглифы)
db = MySQLdb.connect(host="localhost", user="root", passwd="123456", db="egorch", charset='utf8')
# формируем курсор, с помощью которого можно исполнять SQL-запросы
cursor = db.cursor()
# проверим таблицы
cursor.execute("SELECT post, author, text, date FROM comments order by post, id")
#cursor.execute("SELECT post, author, text, date FROM comments order by post, id limit 1")


for row in cursor.fetchall():
    comment = list(row)
    
    try:
        data = Post.objects.filter(url=str(comment[0]))[0] 
        if data.id > 0:
            print(data.id)  
            q = CommentPost()    
            q.post_id = data.id
            q.name_user = comment[1]
            #очистка от спецсимволов
            #text = re.sub(r'[^\w\s.,]', '', comment[2])
            text = comment[2].replace('&gt', '').replace('&quot;', '')
            q.text = text
            q.create_date = datetime.strptime(str(comment[3]), '%Y-%m-%d')    
            
            #print(comment)
            print(q)
            q.save()
    except IndexError as msg:
        print(msg)
        
    
db.close()