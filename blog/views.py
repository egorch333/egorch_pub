from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect, QueryDict
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic import CreateView
from django.views.generic.edit import FormView
from django.views.generic import View
from django.views.generic.base import RedirectView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.contrib import messages
from django.urls import reverse_lazy
from .form import *
from django.core.serializers import serialize
import math
import json
from datetime import datetime as dt, timedelta
from django.utils import timezone

from captcha.models import CaptchaStore
from captcha.helpers import captcha_image_url

from .models import *
"""
class QuestionView(generic.DetailView):
    model = Question
    template_name = 'blog/index.html'

class IndexView(generic.DetailView):
    model = PageMenu
    template_name = 'blog/index.html'
    
    '''
    def get(self, request, *args, **kwargs):
        return HttpResponse('Hello, World!')
    
    '''
    def get_queryset(self):        
        return PageMenu.objects.filter(url='index')
        #return self.get()
        
class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def index(request):
    index_list = Page.objects.filter(url='index')[0]
    context = {'index_list': index_list}
    print(context)
    return render(request, 'blog/index.html', context)
    
def page(request, url):
    #index_list = Page.objects.filter(url=url)[0]
    '''
    get_object_or_404 - проверяет есть ли такая запись,
    если нет, то переходим на старницу 404
    
    render - соединяет шаблон и контекст(словарь)
    '''
    index_list = get_object_or_404(Page, url=url)
    context = {'index_list': index_list}
    print(context)
    return render(request, 'blog/index.html', context)
    
def category(request, url):
    print(url)
    category_id = get_object_or_404(Category, url=url)
    post_list = Post.objects.filter(category_id=category_id).order_by('title')
    context = {'post_list': post_list}
    return render(request, 'blog/category.html', context)
    
def category_all(request):
    category_list = Category.objects.order_by('title')
    context = {'category_list': category_list}
    print(context)
    return render(request, 'blog/category_all.html', context)   
    
def post(request, url): 
    post_list = get_object_or_404(Post, url=url)
    context = {'post_list': post_list}
    print(post_list)
    return render(request, 'blog/post.html', context)     
"""

class MenuMixin:
    #блок названия категрий
    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        context['menu_mixin'] = Page.objects.exclude(url='index').exclude(url='contact').exclude(url='sitemap').order_by('position')
        return context

class CategoryMixin:
    #блок названия категрий
    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        context['category_mixin'] = Category.objects.order_by('title')
        return context
    
class PostTopMixin:
    #блок названия категрий
    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        context['post_top_mixin'] = Post.objects.filter(visible=True).order_by('-create_date')[:5]
        return context   

class RatingMixin:
    #блок названия категрий
    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        context['rating_mixin'] = Category.objects.order_by('title')
        return context


class FooterMixin:
    #блок фуутер
    def get_context_data(self, **kwargs):
        #index_list = Page.objects.filter(url='index')[0]
        #index_list = {'text': index_list }
        context = super().get_context_data(**kwargs)
        #данные для футера
        footer = {}
        #можно использовать datetime.datetime.today() встроенный по умолчанию
        footer['year_create'] = dt.today()
        #отображаю только видимые статьи
        footer['count_article'] = Post.objects.filter(visible=True).count()
        footer['comment_article'] = CommentPost.objects.count()
        footer['online_user'] = OnlineUser.objects.count() 
        context['footer_mixin'] = footer
        return context 
    
class SitesFriendsMixin:
    #сайты друзей
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)        
        context['sites_friends_mixin'] = SitesFriends.objects.filter(visible=True)
        return context     

class IndexView(MenuMixin, CategoryMixin, PostTopMixin, RatingMixin, FooterMixin, SitesFriendsMixin, TemplateView):
    model = Page
    template_name = 'blog/page.html'
    context_object_name = 'index_list'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        data = self.model.objects.filter(url='index')[0]
        view_page = data.view_page + 1
        #обновление просмотров
        self.model.objects.filter(id=data.id).update(view_page=view_page)
        context['index_list'] = data
        return context

class PageView(MenuMixin, CategoryMixin, PostTopMixin, RatingMixin, FooterMixin, SitesFriendsMixin, DetailView):
    model = Page
    template_name = 'blog/page.html'
    context_object_name = 'index_list'
    slug_field = 'url'
    slug_url_kwarg = 'url'
    
    def get_queryset(self):
        #расширяет функцию
        qs = super().get_queryset()
        url = self.kwargs['url']             
        data = self.model.objects.filter(url=url)[0]
        view_page = data.view_page + 1
        self.model.objects.filter(id=data.id).update(view_page=view_page)
        return qs

    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        #url = self.kwargs['url']        
        return context


class CategoryView(MenuMixin, CategoryMixin, PostTopMixin, RatingMixin, FooterMixin, SitesFriendsMixin, ListView):
    model = Post
    template_name = 'blog/category.html'
    context_object_name = 'page_list'
    #slug_field = 'url'
    paginate_by = 5
    
    def get_queryset(self):
        #расширяет функцию
        qs = super().get_queryset()
        url = self.kwargs['url']
        self.category = Category.objects.filter(url=url)[0]
        qs = qs.filter(visible=True, category_id=self.category.id).order_by('create_date')
        #qs = qs.filter(visible=True, category_id=self.category.id).order_by('id')
        return qs

    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        context['category_list'] = self.category
        return context


class PostView(MenuMixin, CategoryMixin, PostTopMixin, RatingMixin, FooterMixin, SitesFriendsMixin, DetailView):
    model = Post
    template_name = 'blog/post.html'
    context_object_name = 'post_list'
    slug_field = 'url'
    slug_url_kwarg = 'url'
    
    def get_queryset(self):
        #расширяет функцию
        qs = super().get_queryset()
        
        return qs

    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        url = self.kwargs['url']
        
        #отображаю только видимые статьи
        data = self.model.objects.filter(url=url, visible=True)[0]
        view_page = data.view_page + 1
        self.model.objects.filter(id=data.id).update(view_page=view_page)
        
        #для хлебных крошек
        context['category_list'] = Category.objects.filter(id=data.category_id)[0]
                
        #комментарии к статье
        context['comment_list'] = CommentPost.objects.filter(post=data.id).order_by('create_date') 
        
        #форма добавления комментария
        url_full = 'http://' + self.request.META['HTTP_HOST'] + self.request.path

        #context['form'] = CommentPostForm(initial={'url': url_full, 'ip': self.request.META['REMOTE_ADDR']})      
        context['form'] = CommentPostForm(initial={'post': data.id})      
        return context

    
class AddCommentPostView(MenuMixin, CategoryMixin, PostTopMixin, RatingMixin, FooterMixin, SitesFriendsMixin, CreateView):
    model = CommentPost
    form_class = CommentPostForm
    template_name = 'blog/add_comment.html'
    #success_url = reverse_lazy('blog:post', kwargs={'url': '19'})  
    success_url = reverse_lazy('blog:add_comment')  
    
    def form_invalid(self, form):
        response = super().form_invalid(form)        
        messages.error(self.request, 'Ошибка! Комментарий не добавлен!')
        return response
    
    def form_valid(self, form):
        response = super().form_valid(form)   
        #отправка сообщения
        host = self.request.META['REMOTE_ADDR']
        if host != '127.0.0.1':
            data = self.request.POST
            send = send_mail(
                'egorch.ru: комментарий к статье ',
                'страница: ' + self.request.META['HTTP_REFERER'] + ', от ' + data['name_user'] + ', текст: \n' + data['text'],
                'egorchles@yandex.ru',
                ['egorchles@yandex.ru'],
                fail_silently=False,
            )           
            if send == True:
                messages.success(self.request, 'Комментарий успешно добавлен и отослан )')
        else:
            messages.error(self.request, 'Комментарий успешно добавлен, но не отослан')
        return response
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)                
        context['ref'] = self.request.META['HTTP_REFERER'] 
        return context
    

class ReplaceCaptchaAjaxView(CreateView):

    def get(self, request):
        to_json_response = dict()
        to_json_response['status'] = 0
        #to_json_response['form_errors'] = form.errors

        to_json_response['new_cptch_key'] = CaptchaStore.generate_key()
        to_json_response['new_cptch_image'] = captcha_image_url(to_json_response['new_cptch_key'])

        return HttpResponse(json.dumps(to_json_response))
        

class SitemapView(MenuMixin, CategoryMixin, PostTopMixin, RatingMixin, FooterMixin, SitesFriendsMixin, TemplateView):
    model = Page
    template_name = 'blog/sitemap.html'
    context_object_name = 'sitemap_list'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        #увеличение просмотров           
        data = self.model.objects.filter(url='sitemap')[0]
        view_page = data.view_page + 1
        print(view_page)
        self.model.objects.filter(id=data.id).update(view_page=view_page)
              
        context['page'] = Page.objects.filter(url='sitemap')[0]
        context['page_list'] = Page.objects.exclude(url='index').exclude(url='contact').exclude(url='sitemap').order_by('title')
        context['category_list'] = Category.objects.order_by('title')               
        context['post_list'] = Post.objects.filter(visible=True).order_by('category', 'title')
        return context
    
class SearchView(MenuMixin, CategoryMixin, PostTopMixin, RatingMixin, FooterMixin, SitesFriendsMixin, TemplateView):
    model = Post
    template_name = 'blog/search.html'
    context_object_name = 'search_list'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        search = self.request.GET['search'].strip() 
        if len(search) > 0:
            context['search_list']  = self.model.objects.filter(text__icontains=search).order_by('title')
            print(context['search_list'])
        return context
    
class ContactView(MenuMixin, CategoryMixin, PostTopMixin, RatingMixin, FooterMixin, SitesFriendsMixin, FormView):
    template_name = 'blog/contact.html'
    form_class = ContactMessageForm
    success_url = reverse_lazy('blog:contact')
    
    def form_valid(self, form):
        #отправка сообщения
        host = self.request.META['REMOTE_ADDR']
        if host != '127.0.0.1':
            data = self.request.POST
            send = send_mail(
                data['select_theme'],
                'от: ' + data['name_user'] + '\n' + data['text'],
                'egorchles@yandex.ru',
                ['egorchles@yandex.ru'],
                fail_silently=False,
            )            
            if send == True:
                messages.success(self.request, 'ваше сообщение отправлено )')
        else:
            messages.error(self.request, 'Ошибка! сообщение не отправлено ) хост: ' + host)            
        return super().form_valid(form)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        #увеличение просмотров           
        data = Page.objects.filter(url='contact')[0]
        view_page = data.view_page + 1
        Page.objects.filter(id=data.id).update(view_page=view_page)
        context['page'] = data        
        return context


class VoteCategoryAjaxView(View):
    marker = "%"
    
    def get(self, request): 
        #изменение рейтинга
        category = request.GET['category']
        data_one = Category.objects.filter(url=category)[0]
        Category.objects.filter(url=category).update(vote = data_one.vote + 1)
        
        #выборка всех статей
        data = Category.objects.order_by("title")
        
        #логика расчёта
        sum_list = [p.vote for p in data]
        num_sum = sum(sum_list)
        one = (num_sum * 1)/100        
        arr = []
        for n in list(data):
            vote = math.floor((n.vote * 100) / num_sum)
            arr.append({'url': n.url, 'title': n.title, 'vote': vote})
        
        #запись в бд
        for n in arr:
            Category.objects.filter(url=n['url']).update(vote = n['vote'])
        
        #если данные из базы,то их нодо сериализовать
        #data = serialize('json', Category.objects.all(), fields=('title','vote','url'))
        data = json.dumps(arr)        
        return HttpResponse(data)
    
class RatingPostView(View):
    '''
    рейтинг для статей post
    '''
    def get(self, request):
        post_id = request.GET['id']
        score = int(request.GET['score'])
        data = Post.objects.filter(id=post_id)[0]
        
        #новый рейтинг
        new_rating = data.rating + score
        new_q_vote = data.q_vote + 1
        rating_result = new_rating/new_q_vote
        rating_result = int(rating_result)
        
        #запись в бд
        data = Post.objects.filter(id=post_id).update(q_vote=new_q_vote, rating=new_rating, rating_result=rating_result,)        
        
        #вывод для страницы
        data = {}
        data['rating_result'] = rating_result
        data = json.dumps(data)
        return HttpResponse(data)

class OnlineUserView(View):
    '''
    подсчёт кол-ва пользователей онлайн
    '''
    def get(self, request):
        #получаем ip пользователя
        ip = request.META.get('HTTP_X_FORWARDED_FOR', request.META['REMOTE_ADDR'])
        
        '''
        timezone.now() - для запросов к бд использовать только timezone.now()
        не использовать datetime.datetime.now(), там проблемы с часовым поясом
        '''
        last_minute = timezone.now() - timedelta(minutes=5)
        OnlineUser.objects.filter(create_date__lt=last_minute).delete()
        data = OnlineUser.objects.filter(ip = ip)
        
        if len(data) > 0:
            if ip == data[0].ip:
                #обновляем данные сессии
                OnlineUser.objects.filter(ip = ip).update(create_date = dt.now())
        else:
            #добавляю сессию
            OnlineUser.objects.create(ip=ip)
        
        #формирование вывода
        data = {}
        data['online_user'] = OnlineUser.objects.count()   
        data = json.dumps(data)
        return HttpResponse(data)
    

class RedirectView(RedirectView):
    permanent = False
    query_string = True
    url = None

    def get_redirect_url(self, *args, **kwargs):        
        site = get_object_or_404(Redirect, title=kwargs['title'])
        #увеличиваю счётчик
        Redirect.objects.filter(id=site.id).update(count_redirect = site.count_redirect+1)
        #автоматически выполняется редирект
        return site.url
        
class RedirectPhpPostView(RedirectView):
    permanent = True
    query_string = True    
    url = None
    
    def get_redirect_url(self):
        '''      
        data = QueryDict(self.request.META['QUERY_STRING'])
        site = get_object_or_404(Post, url=data['id'])        
        #автоматически выполняется редирект
        url = '/post/' + site.url
        return url
        '''
        data = QueryDict(self.request.META['QUERY_STRING'])
        id = int(data['id'])
        if id >= 55:
           id -= 1       
        site = get_object_or_404(Post, id=id)
        
        #print(id)            
        #return '/post/' + data['id']
        return '/post/' + site.url

class RedirectPhpCategoryView(RedirectView):
    permanent = True
    query_string = True    
    url = None
    
    def get_redirect_url(self):
        data = QueryDict(self.request.META['QUERY_STRING'])
        id = data['cat']
        if id == '14': 
            id = 13
        site = get_object_or_404(Category, id=id)        
        #автоматически выполняется редирект
        url = '/category/' + site.url
        return url


class RedirectWsgiView(RedirectView):
    permanent = True
    query_string = True    
    url = None
    
    def get_redirect_url(self):
        url = self.request.META['PATH_INFO']
        url = url.replace('/django.wsgi', '')        
        return url