# -*- coding: utf-8

import MySQLdb
import re
import os
from .models import *

import pytz
from datetime import datetime



#подключение
'''
python manage.py shell
import blog.replace_miniicon

Скрипт для копирования статей post с удалённого сервера
exit()
'''

#подключён файл
print('подключён файл: ' + __file__)

rows = Post.objects.all()

for row in rows:
    
    icon = str(row.mini_icon)
    icon = icon.split('/')
    icon = icon.pop()
    url = '/post/' + row.url + '/' + icon 
    print(url)
    
    Post.objects.filter(url=row.url).update(mini_icon=url)
    