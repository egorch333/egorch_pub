from django.contrib.sitemaps import Sitemap
from blog.models import Post


class BlogSitemap(Sitemap):
    '''
    changefreq - Вероятная частота изменения этой страницы. 
    Это значение предоставляет общую информацию для поисковых систем и 
    может не соответствовать точно частоте сканирования этой страницы. 
    Допустимые значения:
    weekly - неделя 
    monthly -месяц
    yearly -  год
    never - никогда
    '''
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        #return Post.objects.filter(is_draft=False)
        return Post.objects.filter(visible=True).order_by('-create_date')
        
    '''
    def lastmod(self, obj):
        return obj.create_date   
    '''