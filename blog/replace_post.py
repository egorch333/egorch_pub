# -*- coding: utf-8

import MySQLdb
import re
import os
from .models import *

import pytz
from datetime import datetime



#подключение
'''
python manage.py shell
import blog.replace_post

Скрипт для копирования статей post с удалённого сервера
exit()
'''

#подключён файл
print('подключён файл: ' + __file__)

rows = Post.objects.order_by('id')

for row in rows:
    
    text = row.text
    '''
    text = text.replace('<p align="center" class="m">','<p class="center">')
    text = text.replace('<p align="center" class="m" >','<p class="center">')
    text = text.replace('<p align="left" class="m">','<p>')
    text = text.replace('<p align="justify" class="m">','<p>')
    text = text.replace('<p align="justify" class="m" >','<p>')
    text = text.replace('<p align="justify" class="m" ">','<p>')
    text = text.replace('<div id="m_img">','<div class="center">')
    text = text.replace('width="50" height="50" class="rss2"','')
    text = text.replace(' width="100" height="96"','')
    text = text.replace(' width="50" height="50"','')
    text = text.replace('<br>','')
    text = text.replace('<br/>','')
    text = re.sub(r'width=\"\w+\" height=\"\w+\"\s?\/?>', '>', text)
    text = text.replace('              <p>','<p>')
    text = text.replace('              <div','<div')
    text = text.replace('              <p','<p')
    text = text.replace('<p class="m">','<p>')
    text = text.replace(' class="m" style="margin-left: 0cm; margin-right: 0cm; margin-top: 0cm"','')
    '''
    text = text.replace('href="post', 'href="/post')
    text = text.replace('href="http://egorch.ru', 'href="')
    print(row.url)
    
    Post.objects.filter(url=row.url).update(text=text)
    