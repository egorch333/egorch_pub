# -*- coding: utf-8 -*-
from django import forms
from captcha.fields import CaptchaField
from .models import CommentPost, StopWord



class ContactMessageForm(forms.Form):
    #error_css_class = 'error'
    #required_css_class = 'required'    
    
    THEME_CHOICES = (
        ('egorch.ru: реклама на сайте', 'реклама на сайте'),
        ('egorch.ru: ошибки на страницах', 'ошибки на страницах'),
        ('egorch.ru: сотрудничество', 'сотрудничество'),
        ('egorch.ru: другое', 'другое'),
    )
    
    select_theme = forms.ChoiceField(
        label='Выберите тему', 
        widget=forms.Select(attrs={'class': 'form-control form-control-sm', 'required': True}, ),
        choices=THEME_CHOICES,
    )
    
    name_user = forms.CharField(
        label='Имя пользователя', 
        max_length=100,
        widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'required': True}),
    )
    
    text = forms.CharField(
        label='Текст сообщения', 
        max_length=2000,
        widget=forms.Textarea(attrs={'class': 'form-control form-control-sm', 'rows': 7, 'required': True})
    )
    
    captcha = CaptchaField(label='капча:')

    
    
class CommentPostForm(forms.ModelForm):
    captcha = CaptchaField(label='капча:')
    '''
    комментарии для статей post
    error_css_class = 'error'
    required_css_class = 'required'
    
    
    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        # adding css classes to widgets without define the fields:
        
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control form-control-sm'
        
        self.fields['text'].widget.attrs['class'] = 'form-control form-control-sm'
    '''
    '''
    post = forms.IntegerField(
        widget=forms.HiddenInput,
    )
    post_id = forms.IntegerField()
    '''
              
    class Meta:
        model = CommentPost
        
        fields = ('name_user', 'text', 'post')
        labels = {
            'name_user': "имя пользователя *",
            'text': "сообщение *",
        }
        # словарь, вспомогательный текст для элементов
        '''
        help_texts = {
            'name_user': 'введите фимилию и имя',
        }
        '''
        widgets = {
            'name_user': forms.TextInput(attrs={'class': 'form-control form-control-sm', 'required': True}),
            'text': forms.Textarea(attrs={'class': 'form-control form-control-sm', 'rows': 7, 'required': True}),
            'post': forms.HiddenInput,
        }
        
    def clean(self):
        '''
        проверка для спама
        подгрузка стоп-слов        
        '''
        stop_word = StopWord.objects.all()[0]
        stop_word = stop_word.text.split('|')
        
        # Определяем правило валидации
        text = self.cleaned_data.get('text').lower()
                
        for stop in stop_word:
            if len(stop) > 0:
                if stop in  text:
                    print(stop, 'найдено')
                    # Выбрасываем ошибку, если есть совпедение
                    raise forms.ValidationError('Вы спамите! Спам и реклама запрещена')
                    return self.cleaned_data
    
    