from django.contrib import admin

from .models import Page, Post, Category, SitesFriends, CommentPost, Redirect, StopWord


class CategoryAdmin(admin.ModelAdmin):
    #вывод названия в таблице
    list_display = ['title', 'vote', 'change_date']
    
    #вывод полей на страницу для редактирования
    fields=['url', 'description', 'keywords', 'title', 'h1_title', 'vote',]
    
    #сортировка по названию
    ordering = ['title']
    
    #поле только для чтения
    #readonly_fields=['vote'] 
    
     
class PageAdmin(admin.ModelAdmin):
    #вывод названия в таблице
    list_display = ['title', 'position', 'view_page', 'change_date']
    
    #вывод полей на страницу для редактирования
    fields=['url', 'description', 'keywords', 'title', 'h1_title', 'text', 'view_page',  'position']
    
    #поле только для чтения
    readonly_fields=['view_page'] 
    
    #сортировка по названию
    ordering = ['position']
    
    list_per_page = 10
   
   
class PostAdmin(admin.ModelAdmin):
    #вывод названия в таблице
    list_display = ['title', 'rating_result', 'view_page', 'visible', 'count_comment', 'change_date', 'image_img']
    
    #вывод полей на страницу для редактирования
    fields=['category', 'url', 'description', 'description_full', 'keywords', 'title', 'h1_title', 'text', 'mini_icon', 'view_page', 'rating_result', 'visible',]
    
    #поле только для чтения
    readonly_fields=['view_page', 'rating_result', 'image_img'] 
    
    #сортировка по названию
    #ordering = ['title']
    ordering = ['id']
    
    #фильтр по категориям
    list_filter=['category_id'] 
    
    #добавил поиск по полю текст
    search_fields=['text']
    
    #постраничная навигация
    list_per_page = 10 
    
      
    
class SitesFriendsAdmin(admin.ModelAdmin):
    #вывод названия в таблице
    list_display = ['text', 'url', 'change_date']
    
    #вывод полей на страницу для редактирования
    fields=['url', 'title', 'text', 'visible']
    
    #сортировка по названию
    ordering = ['text']    
    
    #постраничная навигация
    list_per_page = 10   
    
class CommentPostAdmin(admin.ModelAdmin):
    #вывод названия в таблице
    list_display = ['name_user', 'text', 'post', 'create_date']
    
    #вывод полей на страницу для редактирования
    fields=['post', 'name_user', 'text', 'create_date']
    
    #сортировка по названию
    ordering = ['-create_date']    
    
    #поле только для чтения
    readonly_fields=['create_date']
    
    #постраничная навигация
    list_per_page = 10 
    
    #фильтр по категориям - доделать
    #list_filter=['post'] 

class RedirectAdmin(admin.ModelAdmin):
    #вывод названия в таблице
    list_display = ['title', 'url', 'count_redirect', 'change_date']
    
    #вывод полей на страницу для редактирования
    fields=['title', 'url', 'description', 'count_redirect', 'create_date', 'change_date']
    
    #сортировка по названию
    ordering = ['title']    
    
    #поле только для чтения
    readonly_fields=['create_date', 'change_date']
    
    #постраничная навигация
    list_per_page = 10 
  
class StopWordAdmin(admin.ModelAdmin):  
    #вывод названия в таблице
    list_display = ['change_date']
    
    #вывод полей на страницу для редактирования
    fields=['text', 'change_date']
    
    #поле только для чтения
    readonly_fields=['change_date'] 

admin.site.register(Page, PageAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(SitesFriends, SitesFriendsAdmin)
admin.site.register(CommentPost, CommentPostAdmin)
admin.site.register(Redirect, RedirectAdmin)
admin.site.register(StopWord, StopWordAdmin)

